defmodule StudyGarden.Patrons.Patron do
  use StudyGarden.BaseSchema

  schema "patrons" do
    field :patreon_id, :string
    field :user_id, :binary_id
    field :patron_tier, Ecto.Enum, values: [:crowdfunder, :auditor, :patron]
    field :access_token, :string
    field :refresh_token, :string
    field :expires_at, :utc_datetime
    timestamps()
  end
end
