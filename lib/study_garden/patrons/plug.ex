defmodule StudyGarden.Patreon.Plug do
  import Plug.Conn

  require Ecto.Query

  alias StudyGarden.Repo
  alias StudyGarden.Patrons.Patron

  def patron_tier_plug(conn, _opts) do
    patron =
      case Patron |> Repo.get_by(user_id: conn.assigns.current_user.id) do
        nil -> {:none}
        patron -> {:some, patron}
      end

    assign(conn, :patron, patron)
  end
end
