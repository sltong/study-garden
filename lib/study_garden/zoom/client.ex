defmodule StudyGarden.Zoom.Client do
  alias StudyGarden.Zoom.TokenManager

  @base_path "https://api.zoom.us/v2/"

  defp auth_headers do
    access_token = TokenManager.get_bearer_token()

    HTTPoison.process_request_headers([
      {:Authorization, "Bearer #{access_token}"},
      {:"Content-Type", "application/json"}
    ])
  end

  def create_meeting(%{user_id: user_id, meeting_topic: meeting_topic}) do
    url = @base_path <> "users/#{user_id}/meetings"

    request_body = %{
      participant_video: true,

      # Attendees register once and can attend any meeting occurrence.
      registration_type: 1,
      auto_recording: "local",
      topic: meeting_topic,

      # A recurring meeting with no fixed time.
      type: 3
    }

    request_body_json = Jason.encode!(request_body)

    response =
      HTTPoison.post!(url, request_body_json, auth_headers())
      |> Map.get(:body)
      |> Jason.decode!()

    response
  end
end
