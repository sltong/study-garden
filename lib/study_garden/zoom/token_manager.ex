defmodule StudyGarden.Zoom.TokenManager do

  @genserver_name :zoom_token_manager

  def start_link(args) do
    GenServer.start_link(StudyGarden.Zoom.TokenManager.Server, args, name: @genserver_name)
  end

  def get_ttl() do
    GenServer.call(@genserver_name, :get_ttl)
  end

  def get_bearer_token() do
    GenServer.call(@genserver_name, :get_bearer_token)
  end

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]},
      type: :worker,
      restart: :permanent,
      shutdown: 500,
    }
  end
end
