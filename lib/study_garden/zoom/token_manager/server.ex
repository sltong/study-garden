defmodule StudyGarden.Zoom.TokenManager.Server do
  use GenServer
  # Server
  @impl true
  def init(state) do
    Process.send(self(), :fetch_access_token, [])
    {:ok, state}
  end

  @impl true
  def handle_call(:get_ttl, _from, state) do
    {:reply, state[:expires_in], state}
  end

  def handle_call(:get_bearer_token, _from, state) do
    {:reply, state[:bearer_token], state}
  end

  @impl true
  def handle_info(:fetch_access_token, state) do
    account_id = Map.get(state, :zoom_account_id)
    client_id = Map.get(state, :zoom_client_id)
    client_secret = Map.get(state, :zoom_client_secret)
    base64_token = Base.encode64("#{client_id}:#{client_secret}")

    headers =
      HTTPoison.process_request_headers([
        {:Authorization, "Basic #{base64_token}"},
        {:"Content-Type", "application/json"}
      ])

    url = "https://zoom.us/oauth/token?grant_type=account_credentials&account_id=" <> account_id

    response =
      HTTPoison.post!(url, "", headers)
      |> Map.get(:body)
      |> Jason.decode!()

    bearer_token = Map.get(response, "access_token")
    expires_in = Map.get(response, "expires_in")

    # re-fetch access_token a minute before it expires, expires_in is in seconds so multiply by 1000
    Process.send_after(self(), :fetch_access_token, (expires_in - 60) * 1000)

    {:noreply, Map.merge(state, %{bearer_token: bearer_token, expires_in: expires_in})}
  end
end
