defmodule StudyGarden.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      StudyGarden.Repo,
      # Start the Telemetry supervisor
      StudyGardenWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: StudyGarden.PubSub},
      # Start the Endpoint (http/https)
      StudyGardenWeb.Endpoint,
      # Start a worker by calling: StudyGarden.Worker.start_link(arg)
      # {StudyGarden.Worker, arg}
    ] ++
      if Application.get_env(:study_garden, Zoom)[:zoom_enabled],
      do:
      [{
        StudyGarden.Zoom.TokenManager,
        Application.get_env(:study_garden, Zoom)
          |> Map.new
          |> Map.take([:zoom_client_id, :zoom_account_id, :zoom_client_secret])
      }],
      else: []

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: StudyGarden.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    StudyGardenWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
