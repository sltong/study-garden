defmodule StudyGarden.Patreon.Client do
  # alias StudyGarden.Patrons.Patron
  # alias StudyGarden.Repo

  def get_patron_info({:access_token, access_token, patreon_id}) do
    headers =
      HTTPoison.process_request_headers([
        {:Authorization, "Bearer #{access_token}"},
        {:"Content-Type", "application/json"}
      ])

    url =
      "https://www.patreon.com/api/oauth2/v2/members/#{patreon_id}?include=currently_entitled_tiers"

    case HTTPoison.get(url, headers) do
      {:ok, res} ->
        {
          :ok,
          res
          |> Map.get(:body)
          |> Jason.decode!()
        }

      {:error, reason} ->
        {:error, reason}
    end
  end

  # def get_patron_info({:user_id, user_id}) do
  # end
end
