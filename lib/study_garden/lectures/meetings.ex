defmodule StudyGarden.Meetings.Meeting do
  # This is a macro wrapper around Ecto.Schema that automatically injects a uuid primary key
  use StudyGarden.Schema

  schema "meetings" do
    timestamps()
  end
end
