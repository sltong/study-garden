defmodule StudyGardenWeb.Router do
  use StudyGardenWeb, :router
  use Pow.Phoenix.Router

  use Pow.Extension.Phoenix.Router,
    extensions: [PowResetPassword, PowEmailConfirmation]

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
      error_handler: Pow.Phoenix.PlugErrorHandler
  end

  pipeline :oauth do
    plug Ueberauth
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {StudyGardenWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  # TODO: Do we really need these? I commented them out and everything works.

  # Override the default Pow routes for user registration and login.
  # These must come before the call to `pow_routes/0`.
  # scope "/", Pow.Phoenix, as: "pow" do
  #   pipe_through :browser
  #
  #   get "/register", RegistrationController, :new
  #   post "/register", RegistrationController, :create
  #
  #   get "/account/edit", RegistrationController, :edit
  #   post "/account/edit", RegistrationController, :edit
  #
  #   get "/login", SessionController, :new
  #   post "/login", SessionController, :create
  # end

  scope "/" do
    pipe_through :browser

    pow_routes()
    pow_extension_routes()
  end

  # External authentication
  # NOTE: unfortunately ueberauth implicitly requires that these routes be prefixed with "auth"
  # I would prefer something like "oauth" but c'est la vie
  scope "/auth", StudyGardenWeb do
    pipe_through :oauth

    # Only allow the patreon provider, the docs suggest using :provider, but that doesn't make sense for us
    # since we don't want to allow requests to say www.study.garden/auth/github
    get "/patreon", OAuthController, :request
    get "/patreon/callback", OAuthController, :callback
  end


  # Protected routes
  scope "/", StudyGardenWeb do
    pipe_through [:browser, :protected]

    get "/dashboard", DashboardController, :index
  end

  # Public routes
  scope "/", StudyGardenWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", StudyGardenWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/live_dashboard", metrics: StudyGardenWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
