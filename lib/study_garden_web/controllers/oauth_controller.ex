defmodule StudyGardenWeb.OAuthController do
  use StudyGardenWeb, :controller
  require Logger
  alias StudyGarden.Patrons.Patron
  alias StudyGarden.Repo
  alias StudyGarden.Patreon.Client, as: Patreon

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate.")
    |> redirect(to: "/dashboard")
  end

  def callback(%{assigns: %{ueberauth_auth: auth, current_user: user}} = conn, _params) do
    Logger.info(user.id)
    Logger.info(auth.credentials.token)
    Logger.info(auth.credentials.refresh_token)
    Logger.info(DateTime.from_unix!(auth.credentials.expires_at))
    Logger.info(auth.uid)

    patron = %Patron{
      patreon_id: auth.uid,
      user_id: user.id,
      patron_tier: nil,
      access_token: auth.credentials.token,
      refresh_token: auth.credentials.refresh_token,
      expires_at: DateTime.from_unix!(auth.credentials.expires_at),
    }

    res = Patreon.get_patron_info({:access_token, auth.credentials.token, auth.uid})

    case res do
      {:ok, body} -> IO.inspect(body)
      {:error, reason} -> Logger.error(reason)
    end

    case Repo.insert patron do
      {:ok, _patron} ->
        conn
        |> put_flash(:info, "Succesfully connected your patreon account")
        |> redirect(to: "/dashboard")

      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: "/dashboard")
    end
  end
end

# INFO: Response structure from ueberauth / patreon

# %Ueberauth.Auth {
#   credentials: %Ueberauth.Auth.Credentials{
#     token: "TQMtm3mmPXhp35ikYv0O5H-uu5uUf98jxROHlWe9S54",
#     refresh_token: "sqF1y_8naJUtj6HiRbi1qPUQymC_1977edL4Lo9Catc",
#     token_type: "Bearer",
#     secret: nil,
#     expires: nil,
#     expires_at: 1674833994,
#     scopes: [],
#     other: %{}
#   },
#   extra: %Ueberauth.Auth.Extra{
#     raw_info: %{
#       "data" => %{
#         "attributes" => %{
#           "about" => nil,
#           "email" => "jonathan_lorimer@mac.com",
#           "first_name" => "Jonathan",
#           "full_name" => "Jonathan Lorimer",
#           "image_url" => "https://c8.patreon.com/2/200/25181473",
#           "last_name" => "Lorimer",
#           "url" => "https://www.patreon.com/user?u=25181473"
#         },
#         "id" => "25181473",
#         "type" => "user"
#       },
#       "links" => %{
#         "self" => "https://www.patreon.com/api/oauth2/v2/user/25181473"
#       }
#     }
#   },
#   info: %Ueberauth.Auth.Info{
#     name: "Jonathan Lorimer",
#     first_name: "Jonathan",
#     last_name: "Lorimer",
#     nickname: nil,
#     email: "jonathan_lorimer@mac.com",
#     location: nil,
#     description: nil,
#     image: "https://c8.patreon.com/2/200/25181473",
#     phone: nil,
#     birthday: nil,
#     urls: %{
#       profile: "https://www.patreon.com/user?u=25181473"
#     }
#   }, provider: :patreon,
#   strategy: Ueberauth.Strategy.Patreon,
#   uid: "25181473
#}
