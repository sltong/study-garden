defmodule StudyGardenWeb.DashboardController do
  use StudyGardenWeb, :controller
  import StudyGarden.Patreon.Plug
  require Logger

  plug :patron_tier_plug

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
