# Study Garden

Study Garden is an educational platform where scholars can give lectures, hold
discussions with students, and seek funding from patrons.

# Project Setup

## Software Installations

> NOTE: if you are familiar with nix you can skip installing elixir and erlang and just run
> `nix develop`

### Install Elixir and Erlang
- [Install elixir for your distribution](https://elixir-lang.org/install.html#distributions)
- [Install erlang](https://elixir-lang.org/install.html#installing-erlang)

### Install Hex

Once elixir and erlang are installed you can run `mix` and it will prompt you to install the `hex` package manager
and run `mix deps.get` to install the projects dependencies.

### Linux

On linux you may need to install [inotify-tools](https://github.com/inotify-tools/inotify-tools) if it is not already
installed. Installation varies depending on your distribution so you will have to look up installation instructions.

### Install postgres
- [Follow the instructions here to install postgres locally](https://www.postgresql.org/download/)
- Check that the postgres server is up by running `postgres --version`
- Check that the psql postgres client is up by running `psql --version`

## Configuration

run `mix setup` to create the `study_garden_dev` database, and run the migrations

## Running

To run the development server us the `mix phx.server` command
