defmodule StudyGarden.Repo.Migrations.CreateUserIdentities do
  use Ecto.Migration

  def change do
    create_query = "CREATE TYPE patron_tier AS ENUM ('crowdfunder', 'auditor', 'patron')"
    drop_query = "DROP TYPE patron_tier"
    execute(create_query, drop_query)

    create table(:patrons, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :patreon_id, :string, null: false
      add :user_id, references("users", on_delete: :nothing, type: :uuid), null: false
      add :patron_tier, :patron_tier
      add :access_token, :string, null: false
      add :refresh_token, :string, null: false
      add :expires_at, :utc_datetime, null: false
      timestamps()
    end

    create unique_index(:patrons, :patreon_id)
    create unique_index(:patrons, :user_id)
  end
end
