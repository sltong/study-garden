defmodule StudyGarden.Repo.Migrations.AddMeetingsTable do
  use Ecto.Migration

  def change do
    create table(:meetings, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :zoom_id, :integer, null: false
      add :start_url, :string, null: false
      add :join_url, :string, null: false
      add :password, :string, null: false
      add :encrypted_password, :string, null: false
      add :topic, :string, size: 2000, null: false
  end
end
