defmodule StudyGarden.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :email, :string, null: false
      add :password_hash, :string, null: false
      add :email_confirmation_token, :string
      add :email_confirmed_at, :utc_datetime
      add :unconfirmed_email, :string
      timestamps()
    end

    create unique_index(:users, [:email])
    create unique_index(:users, [:email_confirmation_token])
  end
end
