{
  description = ''
    Study Garden is an educational platform where scholars can give lectures, 
    hold discussions with students, and seek funding from patrons.
  '';

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;
    flake-utils.url = github:numtide/flake-utils;
    devshell.url = "github:numtide/devshell";
    devshell.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { nixpkgs, flake-utils, devshell, self }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ devshell.overlay ];
        };
      in
      {
        devShells.default = pkgs.devshell.mkShell {
          commands = [
            {
              help = "Sample command";
              name = "sample";
              command = "echo \"Thanks for checking out our devshell!\"";
              category = "sample";
            }
          ];
          devshell = {
            name = "study-garden";
            startup.local-mix.text = ''
              mkdir -p .nix-mix
              mkdir -p .nix-hex
              export MIX_HOME=$PWD/.nix-mix
              export HEX_HOME=$PWD/.nix-hex
              export PATH=$MIX_HOME/bin:$PATH
              export PATH=$HEX_HOME/bin:$PATH
              export LANG=en_US.UTF-8
              export ERL_AFLAGS="-kernel shell_history enabled"
            '';
            packages = with pkgs; [
              inotify-tools
              glibcLocales
              elixir_1_14
              erlang
              elixir_ls
            ];
          };
        };
      });
}
