import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :study_garden, StudyGarden.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  database:
    System.get_env("POSTGRES_DB") || "study_garden_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: System.get_env("POOL_SIZE") || 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :study_garden, StudyGardenWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "ASpRn3KxbZhKBzSHwgni3zhDgp71eulQ+i3v3lx74mEtVDTGsda2nI2Z9qVdi0OR",
  server: false

# In test we don't send emails.
config :study_garden, StudyGarden.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :study_garden, Zoom,
  zoom_enabled: false
