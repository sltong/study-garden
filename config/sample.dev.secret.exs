import Config

# Configuration for the mimbres school zoom api
config :study_garden, Zoom,
  zoom_webhooks_secret_token: "fake-token",
  zoom_webhooks_verification_token: "fake-token",
  zoom_account_id: "fake-account-id",
  zoom_client_id: "fake-client-id",
  zoom_client_secret: "fake-client-secret"

config :ueberauth, Ueberauth.Strategy.Patreon.OAuth,
  client_id: "fake-client-id",
  client_secret: "fake-client-secret",
  redirect_uri: "http://localhost:4000/auth/patreon/callback"
