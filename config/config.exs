# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :study_garden,
  ecto_repos: [StudyGarden.Repo]

# Configures the endpoint
config :study_garden, StudyGardenWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: StudyGardenWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: StudyGarden.PubSub,
  live_view: [signing_salt: "S9qgz9GS"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :study_garden, StudyGarden.Mailer, adapter: Swoosh.Adapters.Local

# Swoosh API client is needed for adapters other than SMTP.
config :swoosh, :api_client, false

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.29",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :study_garden, :pow,
  user: StudyGarden.Users.User,
  repo: StudyGarden.Repo,
  web_module: StudyGardenWeb,
  mailer_backend: StudyGardenWeb.Pow.Mailer,
  routes_backend: StudyGardenWeb.Pow.Routes,
  extensions: [PowResetPassword, PowEmailConfirmation, PowPersistentSession],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks

config :ueberauth, Ueberauth,
  providers: [
    patreon: {Ueberauth.Strategy.Patreon, [default_scope: "identity.memberships identity campaigns.members"]}
  ]

config :study_garden, StudyGardenWeb.Pow.Mailer,
  adapter: Swoosh.Adapters.Logger,
  level: :debug

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
