# Changelog
All notable changes to this project shall be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Patreon OAuth2 authentication.
- User account registration with an email and password.

[Unreleased]: https://gitlab.com/study-garden/study-garden/compare/v0.0.1...HEAD
